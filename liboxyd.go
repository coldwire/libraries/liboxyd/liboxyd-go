package liboxyd

/*
#cgo LDFLAGS: ${SRCDIR}/liboxyd_go.a -ldl
#include "wrapper.h"
*/
import "C"
import (
	"errors"
	"strings"
	"unsafe"
)

/* ================ UTILS FUNCTIONS ================ */

func parseError(err string) error {
	if strings.HasPrefix(err, "Error:") {
		return errors.New(err)
	}

	return nil
}

func cBytesToGoByteArray(data C.struct_Vec_uint8) []byte {
	return C.GoBytes(unsafe.Pointer(data.ptr), C.int(data.len))
}

func cBytesToGoString(data C.struct_Vec_uint8) string {
	inBytes := cBytesToGoByteArray(data)
	return string(inBytes)
}

func byteToUchar(s []byte) *C.uchar {
	if len(s)+1 <= 0 {
		panic("string too large")
	}

	p := unsafe.Pointer(&s[0])

	sliceHeader := struct {
		p   unsafe.Pointer
		len int
		cap int
	}{
		p,
		len(s) + 1,
		len(s) + 1,
	}

	b := *(*[]byte)(unsafe.Pointer(&sliceHeader))
	copy(b, s)

	b[len(s)] = 0
	return (*C.uchar)(p)
}

func byteToVec(data []byte) C.struct_Vec_uint8 {
	return C.struct_Vec_uint8{
		ptr: byteToUchar(data),
		len: C.ulong(len(data)),
	}
}

/* ================ LIBOXYD FUNCTIONS ================ */

func Secp256k1_generate_keypair() (string, string) {
	var keypair = C.generate_keypair()
	return cBytesToGoString(keypair.secret_key), cBytesToGoString(keypair.public_key)
}

func Secp256k1_sign(secretKey string, data string) (string, error) {
	signature := C.sign_data(byteToVec([]byte(secretKey)), byteToVec([]byte(data)))
	res := cBytesToGoString(signature)

	err := parseError(res)
	if err != nil {
		return "", err
	}

	return res, nil
}

func Secp256k1_verify(publicKey string, signature string, data string) (bool, error) {
	res := C.check_signature(byteToVec([]byte(publicKey)), byteToVec([]byte(signature)), byteToVec([]byte(data)))

	err := parseError(cBytesToGoString(res.message))
	if err != nil {
		return false, err
	}

	return bool(res.is_valid), nil
}

func Secp256k1_revover_public_key(signature string, data string) (string, error) {
	res := C.recover_public_key(byteToVec([]byte(signature)), byteToVec([]byte(data)))
	publicKey := cBytesToGoString(res)

	err := parseError(publicKey)
	if err != nil {
		return "", err
	}

	return publicKey, nil
}

func Base64_encode(data string) string {
	b64 := C.base64_encode(byteToVec([]byte(data)))
	return cBytesToGoString(b64)
}

func Base64_decode(data string) []byte {
	b := C.base64_encode(byteToVec([]byte(data)))
	return cBytesToGoByteArray(b)
}
