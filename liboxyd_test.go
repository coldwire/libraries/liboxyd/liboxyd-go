package liboxyd_test

import (
	"log"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/coldwire/libraries/liboxyd/liboxyd-go"
)

func Test(t *testing.T) {
	var TEST_DATA = liboxyd.Base64_encode("Hello world ! :D")
	var TEST_SIGNATURE_DATA = "SGVsbG8gd29ybGQgISA6RA=="
	var TEST_SIGNATURE_SIGNED = "0DETtXZp9YY8CYcyM838aoB+6pci2xEA0Fg4yxu9S/k6I82AwgjDY+MdCj88qXDVFgmFZMva2fSdaMUkDydSfwA="
	var TEST_SIGNATURE_PKEY = "BDrMu9cGvRwNscn1WtHVMKgvRzcSh0S6zfkGdy1cL8fwuUyheEnVf4VYa24Co4SsQ7bMULg8bufBoWXU2vhe37M="
	var TEST_SIGNATURE_SKEY = "6xFOVLbiin7qasuPEY8kSXI7DpTsy6je7Lfb8eFR1kA="

	signature, err := liboxyd.Secp256k1_sign(TEST_SIGNATURE_SKEY, TEST_DATA)
	if err != nil {
		log.Println(err)
	}

	assert.Equal(t, signature, TEST_SIGNATURE_SIGNED, "Both signature should be equal")

	isValid, err := liboxyd.Secp256k1_verify(TEST_SIGNATURE_PKEY, TEST_SIGNATURE_SIGNED, TEST_SIGNATURE_DATA)
	if err != nil {
		log.Println(err)
	}

	log.Println("Is signature valid ? :", isValid)

	publicKey, err := liboxyd.Secp256k1_revover_public_key(signature, TEST_DATA)
	if err != nil {
		log.Println(err)
	}

	assert.Equal(t, publicKey, TEST_SIGNATURE_PKEY, "public keys should be the same")

}
