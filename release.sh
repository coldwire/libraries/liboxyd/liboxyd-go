#/bin/sh

# Build static library
cargo build --release

# Generate wrapper.h
cargo test

# Copy library to root dir
cp target/release/liboxyd_go.a .
