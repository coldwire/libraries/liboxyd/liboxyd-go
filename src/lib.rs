use safer_ffi::prelude::*;

use liboxyd::{
    secp256k1::{ecdsa, keys, libsecp256k1::PublicKey},
    utils::base64,
};

#[derive_ReprC]
#[repr(C)]
pub struct Keypair {
    secret_key: repr_c::String,
    public_key: repr_c::String,
}

#[ffi_export]
pub fn generate_keypair() -> Keypair {
    let (sk, pk) = keys::generate_keypair();

    Keypair {
        secret_key: repr_c::String::from(sk),
        public_key: repr_c::String::from(pk),
    }
}

#[ffi_export]
pub fn sign_data(secret_key: repr_c::String, data: repr_c::String) -> repr_c::String {
    let data = match base64::decode(data.to_string()) {
        Ok(d) => d,
        Err(e) => return repr_c::String::from(format!("Error: {:?}", e.to_string())),
    };

    let signature = match ecdsa::sign(secret_key.to_string(), &data) {
        Ok(s) => s,
        Err(e) => return repr_c::String::from(format!("Error: {:?}", e.to_string())),
    };

    repr_c::String::from(signature)
}

#[ffi_export]
pub fn recover_public_key(signature: repr_c::String, data: repr_c::String) -> repr_c::String {
    let data = match base64::decode(data.to_string()) {
        Ok(d) => d,
        Err(e) => return repr_c::String::from(format!("Error: {:?}", e.to_string())),
    };

    let public_key = match ecdsa::recover_public_key(signature.to_string(), &data) {
        Ok(pk) => pk,
        Err(e) => return repr_c::String::from(format!("Error: {:?}", e.to_string())),
    };

    let encoded_pk = base64::encode(&public_key.serialize());

    repr_c::String::from(encoded_pk)
}

#[derive_ReprC]
#[repr(C)]
pub struct CheckResult {
    is_valid: bool,
    message: repr_c::String,
}

#[ffi_export]
pub fn check_signature(
    public_key: repr_c::String,
    signature: repr_c::String,
    data: repr_c::String,
) -> CheckResult {
    let data = match base64::decode(data.to_string()) {
        Ok(d) => d,
        Err(e) => {
            return CheckResult {
                is_valid: false,
                message: repr_c::String::from(format!("Error: {:?}", e.to_string())),
            }
        }
    };

    match ecdsa::verify(public_key.to_string(), signature.to_string(), &data) {
        Ok(is_valid) => CheckResult {
            is_valid: is_valid,
            message: repr_c::String::from(format!("")),
        },
        Err(e) => CheckResult {
            is_valid: false,
            message: repr_c::String::from(format!("Error: {:?}", e.to_string())),
        },
    }
}

#[ffi_export]
pub fn base64_encode(data: repr_c::String) -> repr_c::String {
    let b64 = base64::encode(data.to_string());
    repr_c::String::from(b64)
}

#[ffi_export]
pub fn base64_decode(data: repr_c::String) -> repr_c::Vec<u8> {
    let b64 = base64::decode(data.to_string()).unwrap();
    repr_c::Vec::from(b64)
}

#[::safer_ffi::cfg_headers]
#[test]
fn generate_headers() -> ::std::io::Result<()> {
    ::safer_ffi::headers::builder()
        .to_file("wrapper.h")?
        .generate()
}
